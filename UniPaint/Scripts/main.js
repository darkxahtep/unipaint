/// <reference path="_references.js" />

"use strict";

var drawer = $("#drawer").get(0);

var ctx = drawer.getContext("2d");
ctx.strokeStyle = "#000000";
ctx.lineJoin = 'round';
ctx.lineCap = 'round';

$("#color").change(function (e) {
    var color = $("#color").val();
    ctx.strokeStyle = color;
    ctx.fillStyle = color;
});

$("#size").change(function (e) {
    var lineWidth = $("#size").val();
    ctx.lineWidth = lineWidth;
    $("#sizeDisplay").val(lineWidth);
});

$("#size").val(1);
$("#size").trigger("change");

function getOffset(el) {
    var _x = 0;
    var _y = 0;
    while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
        _x += el.offsetLeft - el.scrollLeft;
        _y += el.offsetTop - el.scrollTop;
        el = el.offsetParent;
    }
    return { top: _y, left: _x };
}


function getCanvasMousePos(event) {
    var rect = getOffset(drawer);

    return {
        x: event.pageX - rect.left,
        y: event.pageY - rect.top
    }
}
function draw(event) {
    var pos = getCanvasMousePos(event);
    ctx.lineTo(pos.x, pos.y);
    ctx.stroke();
}


drawer.onmousedown = function (event) {
    var pos = getCanvasMousePos(event);
    ctx.beginPath();
    ctx.moveTo(pos.x, pos.y);
    drawer.onmousemove = draw;
}

drawer.onmouseup = function () {
    ctx.closePath();
    drawer.onmousemove = null;
}
