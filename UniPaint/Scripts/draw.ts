/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="typings/bootstrap/bootstrap.d.ts" />
/// <reference path="typings/signalr/signalr.d.ts" />

class Coords {
    x: number;
    y: number;
    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }
}

interface IDrawable {
    color: string;
    width: number;
    Draw(cntx: CanvasRenderingContext2D): void;
}
interface IDrawer {
    Draw(drawObject: IDrawable): void;
}


interface IDrawerHubConnection extends HubConnection {
    client: {
        addDraw: (drawObject: UniPaint.Line[]) => void;
    }
    server: {
        sendDraw: (drawObject: UniPaint.Line[]) => void;
    }
}

interface SignalR {
    drawerHub: IDrawerHubConnection;
}

// Module
module UniPaint {

    export class Drawer implements IDrawer {
        canvas: HTMLCanvasElement;
        cntx: CanvasRenderingContext2D;
        canvasOffset: Coords;
        drawData: Line[] = [];
        GetOffset(el: HTMLElement): Coords {
            var _x = 0;
            var _y = 0;
            while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
                _x += el.offsetLeft - el.scrollLeft;
                _y += el.offsetTop - el.scrollTop;
                el = <HTMLElement> el.offsetParent;
            }
            return new Coords(_x, _y);
        }
        GetCanvasMousePos(event: MouseEvent): Coords {
            var x = event.pageX - this.canvasOffset.x,
                y = event.pageY - this.canvasOffset.y;
            return new Coords(x, y);
        }

        OnMouseDown(event: MouseEvent, self: Drawer): void {
            var position = self.GetCanvasMousePos(event);
            var line = new Line();
            line.startPos = position,
            line.endPos = position,
            line.color = self.cntx.strokeStyle,
            line.width = self.cntx.lineWidth
            self.Draw(line);
            self.drawData = [];
            self.drawData.push(line);
            self.canvas.onmousemove = (event) => { self.OnMouseMove(event, self); };
        }

        OnMouseMove(event: MouseEvent, self: Drawer): void {
            var position = self.GetCanvasMousePos(event);
            var line = new Line();
            var drawDataLength = self.drawData.length;
            line.startPos = self.drawData[drawDataLength - 1].endPos,
            line.endPos = position,
            line.color = self.cntx.strokeStyle,
            line.width = self.cntx.lineWidth;
            if (drawDataLength >= 15) {
                $.connection.drawerHub.server.sendDraw(self.drawData);
                self.drawData = [];
            }
            self.Draw(line);
            self.drawData.push(line);
        }
        OnMouseUp(event: MouseEvent, self: Drawer): void {
            $.connection.drawerHub.server.sendDraw(self.drawData);
            self.canvas.onmousemove = null;
        }

        constructor(canvas: HTMLCanvasElement) {
            this.cntx = canvas.getContext("2d");
            this.cntx.lineCap = 'round';
            this.cntx.lineJoin = 'round';
            this.canvasOffset = this.GetOffset(canvas);
            this.canvas = canvas;
            canvas.onmousedown = (event: MouseEvent) => { return this.OnMouseDown(event, this) };
            canvas.onmouseup = (event: MouseEvent) => { return this.OnMouseUp(event, this) };
        }

        Draw(drawObject: Line) {
            drawObject.Draw(this.cntx);
        }
    }

    export class Line implements IDrawable {
        startPos: Coords;
        endPos: Coords;
        color: string;
        width: number;

        constructor();
        constructor(drawData: Line);
        constructor(drawData?: any) {
            if (drawData) {
                this.startPos = drawData.startPos;
                this.endPos = drawData.endPos;
                this.color = drawData.color;
                this.width = drawData.width;
            }
        }
        Draw(cntx: CanvasRenderingContext2D) {
            var
                oldColor = cntx.strokeStyle,
                oldWidth = cntx.lineWidth;

            cntx.beginPath();
            cntx.strokeStyle = this.color;
            cntx.lineWidth = this.width;
            cntx.moveTo(this.startPos.x, this.startPos.y);
            cntx.lineTo(this.endPos.x, this.endPos.y);
            cntx.stroke();
            cntx.closePath();
            cntx.lineWidth = oldWidth;
            cntx.strokeStyle = oldColor;
        }
    }
}

// Local variables
var canvas = <HTMLCanvasElement> document.getElementById("drawer");
var drawer = new UniPaint.Drawer(canvas);

$("#color").change(() => {
    drawer.cntx.strokeStyle = drawer.cntx.fillStyle = $("#color").val();
});

var size = $("#size");
size.change(() => {
    var size = $("#size").val();
    $("#sizeDisplay").val(size);
    drawer.cntx.lineWidth = size;
});

$("#color").val("#000000");
size.val("1");
$("#color, #size").trigger("change");



$.connection.drawerHub.client.addDraw = (drawData: UniPaint.Line[]) => {
    for (var i = 0, length = drawData.length; i < length; i++) {
        drawer.Draw(new UniPaint.Line(drawData[i]));
    }
};
$.connection.hub.start()
    .done(function () { console.log("Now connected!"); })
    .fail(function () { console.log("Could not Connect!"); });