﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UniPaint.Controllers {
    public class DrawController : Controller {
        public ActionResult Index() {
            var guid = Guid.NewGuid();
            return RedirectToAction("Drawer", new {
                id = guid
            });
        }

        public ActionResult Drawer(Guid id) {
            ViewBag.Title = "UniPainter";
            ViewBag.Guid = id;
            return View("Index");
        }
    }
}