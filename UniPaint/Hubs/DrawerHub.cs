﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;

namespace UniPaint.Hubs {
    public class DrawerHub : Hub {
        public void SendDraw(dynamic drawObject) {
            Clients.Others.addDraw(drawObject);
        }
    }

    public class Startup {
        public void Configuration(IAppBuilder app) {
            app.MapSignalR();
        }
    }
}